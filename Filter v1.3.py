import os , re 
from email.utils import parseaddr
from utils.countries import *

def check_file():
    file_location = input("Enter the name of the file : ")
    if os.path.isfile(file_location):
        if file_location[-4:] == ".txt":
            print("Start filtring the {} list.".format(file_location[:-4]))
            return file_location
        else:
            print("Invalid format, don't forget the (.txt) extension !\n")
            check_file()
    else:
        print("This file doesn't exist in this directory!\n")
        check_file()

def is_ascii(s):
	# return true if the line doesn't contain bad caracteres
	# and false if does
    return all(ord(c) < 128 for c in s)


def get_key(val): 
    for key, value in local_domaines.items(): 
        if val in value:
            return key
        else:
            None


def getSize(fileobject):
    with open(fileobject, "a") as file:
        file.seek(0,2) # move the cursor to the end of the file
        size = file.tell()
    return size
    

def writeoption(text, filename):
    filename.write(text)

        

def getNewFile(filename, text, size):
    
    if size < 10000000:
        with open(filename, "a") as file_exist:
            writeoption(text, file_exist)
            
    elif size >= 10000000 and "part" in filename:
        # when the file exeed 10 mb it will create new file 
        # for exemple germany part 2 
        part_nbr = re.findall('[0-9]+', filename)[0]
        new_part_name = filename.replace(part_nbr, str(int(part_nbr+1))) 
        filename = new_part_name
        with open(filename, "a") as file_exist:
            writeoption(text, file_exist)

    elif "part" in filename and size < 10000000:
        with open(filename, "a") as file_exist:
            writeoption(text, file_exist)
            

    elif size >= 10000000 and "part" not in filename:
        # when the file exeed 10 mb it will create new file part 1
        filename = filename[:-4] + " part 1.txt"
        with open(filename, "a") as new_file: # new file name part 1
            writeoption(text, new_file)
            
        
    







template = "{} was added to {}"      


def filteremails(filelocation):
    try:

        with open(filelocation, "r") as emails:
            contain = emails.read()
            email_and_pass = contain.splitlines()
    except:

        with open(filelocation, "r",encoding="utf8", errors='ignore') as emails:
            contain = emails.read()
            email_and_pass = contain.splitlines() #
    os.chdir(folder)
    for line in email_and_pass:
        if is_ascii(line):
            email = parseaddr(line)[1].lower()
            domaine_name = email.split('@')[1].lower()
            email_extension = domaine_name.split(".")[-1]

        
        
            if get_key(domaine_name):
                f_name = countries[get_key(domaine_name)] + ".txt" # file name
                getNewFile(f_name, line+'\n',getSize(f_name))
                print(template.format(line,countries[get_key(domaine_name)]))


            elif email_extension in countries: # if exists the code country of the email 
                f_name = countries[email_extension] + ".txt" # file name
                getNewFile(f_name, line+'\n',getSize(f_name))
                print(template.format(line,countries[email_extension] ))
            else:
                f_name = "Other.txt" # file name
                if getSize(f_name) < 10000000:
                    getNewFile(f_name, line+'\n',getSize(f_name))
                    print(template.format(line,"Other"))
                else: 
                    getNewFile(f_name, line+'\n',getSize(f_name))
                    print(template.format(line,"Other"))

        else:
            getNewFile("invalid format.txt", line+'\n',getSize("invalid format.txt"))
            print(template.format(line,"invalid format"))
  

folder = "Email Filtred"

if os.path.isdir(folder):
    pass
else:
    os.mkdir(folder)



                
filteremails(check_file())
